<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Remark;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function store(Request $req) {
        $data = new Remark;
        $data->remarks = $req->remarks;
        $data->save();
        return redirect('home');
        // return $req->input();
    }
}
